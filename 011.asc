// 1459
//== Omake Files 1, 2, 3
== Розділ 11. Омаке-файли №№ 1, 2, 3

// 1460
//Hail the Dark Lord Rowling.
Слава темному лордові Ролінґ!

// 1461
//“Omake” is a non-canonical extra.
"`Омаке`" (яп. オマケ, букв. доповнення) -- це неканонічне доповнення.

***

// 1462
//OMAKE FILES #1: 72 Hours to Victory
.ОМАКЕ-ФАЙЛ №1: 72 години до перемоги

// 1463
//(A.k.a. “What Happens If You Change Harry But Leave All Other Characters
//Constant”)
[center]#(або "`Що як змінити Гаррі, але залишити решту персонажів незмінними`")#

// 1464
//Dumbledore peered over his desk at young Harry, twinkling in a kindly sort of
//way. The boy had come to him with a terribly intense look on his childish face
//- Dumbledore hoped that whatever this matter was, it wasn’t __too__
//serious. Harry was far too young for his life trials to be starting already.
//“What was it you wished to speak to me about, Harry?”
Дамблдор подивився на маленького Гаррі поверх свого стола, його очі доброзичливо
мерехтіли. Хлопчик прийшов до нього з надзвичайно напруженою міною на своєму дитячому
обличчі. Дамблдор сподівався, що хай якою була причина цього візиту, вона не виявиться аж
__надто__ серйозною. Гаррі ще занадто юний для зустрічі з життєвими випробуваннями.

-- Про що ти бажав поговорити зі мною, Гаррі?

// 1465
//Harry James Potter-Evans-Verres leaned forward in his chair, smiling grimly.
//“Headmaster, I got a sharp pain in my scar during the Sorting Feast.
//Considering how and where I got this scar, it didn’t seem like the sort of
//thing I should just ignore. I thought at first it was because of Professor
//Snape, but I followed the Baconian experimental method which is to find the
//conditions for both the presence and the absence of the phenomenon, and I’ve
//determined that my scar hurts if and only if I’m facing the back of Professor
//Quirrell’s head, whatever’s under his turban. While it __could__ be
//something more innocuous, I think we should provisionally assume the worst,
//that it’s You-Know-Who - wait, don’t look so horrified, this is actually a
//priceless opportunity -”
Гаррі Джеймс Поттер-Еванс-Веррес нахилився вперед у своєму кріслі й
похмуро всміхнувся:

-- Директоре, під час бенкету на честь сортування я відчув гострий біль у
шрамі. З огляду на те, де і як я його здобув, я подумав, що це не та річ,
якою можна знехтувати. Спершу мені здалося, що це через професора Снейпа, але,
використовуючи Бе{stress}конівський експериментальний метод, який полягає в пошуку умов як
// @ Baconian experimental method - можливі зміни, якщо знайдено офіційний переклад
для наявности, так і для відсутности феномену, я визначив, що мій шрам болить тоді й
лише тоді, коли я дивлюся на потилицю професора Квірела, хай що там у нього під тюрбаном.
Хоча це __може__ бути чимось неважливим, вважаю, що нам варто тимчасово припустити
найгірше, а саме -- що це Відомо-Хто... Без паніки! Насправді, це неоціненна
нагода…

***

// 1466
//OMAKE FILES #2: I Ain’t Afraid of Dark Lords
.ОМАКЕ-ФАЙЛ №2: Темних лордів я не боюся

// 1467
//This was the original version of Chapter 9. It was replaced because - while
//many readers did enjoy it - many other readers had __massive__ allergies
//to songs in fanfics, for reasons that should not much need belaboring. I didn’t
//want to drive readers away before they got to Ch. 10.
Це початкова версія дев’ятого розділу. Її було змінено, адже попри те, що багатьом
читачам вона сподобалася, у безлічі інших виявилася __жахлива__ алергія на пісні
у фанфіках із причин, що не потребують пояснення. А я не хотів би втрачати читачів,
перш ніж вони дістануться десятого розділу.

// 1468
//Lee Jordan is the fellow prankster of Fred and George (in canon). “Lee Jordan”
//had sounded like a Muggleborn name to me, implying that he would be capable of
//instructing Fred and George on a tune that Harry would know. This was not as
//obvious to some readers as it was to your author.
Лі Джордан (згідно з каноном) -- співучасник розіграшів Фреда й Джорджа. Його ім’я
здалося мені маґлівським, припускаю, він здатен навчити близнюків Візлі мотиву,
що його впізнає Гаррі. Деяким читачам це було менш очевидно, ніж вашому авторові.

***

// 1469
//Draco went to Slytherin, and Harry breathed a small sigh of relief. It had
//__seemed__ like a sure thing, but you never did know what tiny event might
//upset the course of your master plan.
// ! копія 1287
Драко пішов до Слизерину, і Гаррі зітхнув із деякою полегкістю. Це
__нібито__ було безсумнівним, проте ніколи не знаєш, яка дрібна подія може
зіпсувати перебіг твого геніального плану.

// 1470
//They were approaching the Ps now…
Вони наближалися до літери "`П`"...

// 1471
//And over at the Gryffindor table, there was a whispered conversation.
Тим часом за ґрифіндорським столом тривало перешіптування:

// 1472
//__“What if he doesn’t like it?”__
-- __А раптом він образиться?__

// 1473
//__“He’s got no right to not like it -__
-- __Не має права ображатися...__

// 1474
//__“- not after the prank he played on -”__
-- __...Не після його витівки з...__

// 1475
//__“- Neville Longbottom, his name was -”__
-- __...Невілом Лонґботомом, так його звали...__

// 1476
//__“- he’s as fair a fair target now as fair can be.”__
-- __...Позбиткуватися саме з нього зараз справедливіше, ніж із будь кого
іншого.__

// 1477
//__“All right. Just make sure you don’t forget your parts.”__
-- __Гаразд. Тільки не забудьте своїх партій.__

// 1478
//__“We’ve rehearsed it often enough -”__
-- __Ми репетирували їх достатньо часто...__

// 1479
//__“- over the last three hours.”__
-- __...Протягом останніх трьох годин.__

// 1480
//And Minerva McGonagall, from where she stood at the speaker’s podium of the
//Head Table, looked down at the next name on her list. __Please don’t let
//him be a Gryffindor please don’t let him be a Gryffindor OH PLEASE don’t let
//him be a Gryffindor…__ She took a deep breath, and called:
Мінерва Макґонеґел, що стояла за катедрою біля вчительського стола, подивилася
на наступне ім’я в списку. "`Будь ласка, тільки не в Ґрифіндор, будь ласка, тільки
не в Ґрифіндор, НУ БУДЬ ЛАСКА, тільки не в Ґрифіндор...`". Вона глибоко вдихнула й
викликала:

// 1481
//“Potter, Harry!”
-- Гаррі Поттер!

// 1482
//There was a sudden silence in the hall as all whispered conversation stopped.
// версія Labinnac: В залі миттєво настала тиша,
// часткова копія 1290
Раптом у залі запанувала тиша, всі перешіптування обірвалися.

// 1483
//A silence broken by a horrible buzzing noise that modulated and changed in
//hideous mockery of musical melody.
Тиша, яку розітнуло жахливе дзижчання, модульоване й мінливе, огидна пародія на мелодію.

// 1484
//Minerva’s head jerked around, shocked, and identified the buzzing noise as
//coming from the Gryffindor direction, where They were __standing on top
//of the table__ blowing into some kind of tiny devices held against Their lips.
//Her hand started to drop to her wand, to __Silencio__ the lot of Them, but
//another sound stopped her.
Шокована Мінерва різко повернула голову й визначила, що це дзижчання долинає з боку ґрифіндорців,
де ВОНИ видерлися на стіл і дули в якісь невеличкі пристрої, притиснуті до ЇХНІХ губ.
Її рука потяглася до палички, щоб накласти на всіх них закляття "`Сіленціо`", та її зупинив
інший звук.

// 1485
//Dumbledore was chuckling.
Дамблдор хихотів.

// 1486
//Minerva’s eyes went back to Harry Potter, who had only just started to step out
//of line before he’d stumbled and halted.
Мінерва знову перевела погляд на Гаррі Поттера, що ледве встиг зробити один крок,
як одразу завагався й остовпенів.

// 1487
//Then the young boy began to walk again, moving his legs in odd sweeping
//motions, and waving his arms back and forth and snapping his fingers, in
//synchrony with Their music.
Та за мить хлопчик пішов далі. Він із якимись дивними ковзаннями рухав ногами,
розмахував руками вперед і назад, та ще й клацав пальцями синхронно з ЇХНЬОЮ музикою.

// 1488
//__To the tune of “Ghostbusters”__
[center]#__На мелодію "`Мисливців на привидів`" __#

// 1489
//__(As performed on the kazoo by Fred and George Weasley,__
[center]#__(Виконувалася на казу Фредом і Джорджем Візлі__,#

// 1490
//__and sung by Lee Jordan.)__
[center]#__на вокалі -- Лі Джордан.)__#

// 1491
//__.__
[center]#__.__#

// 1492
//__There’s a Dark Lord near?__
[center]#__Поруч темний лорд?__#

// 1493
//______Got no need to fear__
[center]#__Не лякай народ__#

// 1494
//__Who you gonna call?__
[center]#__Хто прийде на поміч?__#

// 1495
//“HARRY POTTER!” shouted Lee Jordan, and the Weasley twins performed a
//triumphant chorus.
-- ГАРРІ ПОТТЕР! -- крикнув Лі Джордан, а брати Візлі продуділи тріумфальний рефрен.

// 1496
//__With a Killing Curse?__
[center]#__Смертельні прокляття?__#

// 1497
//__Well it could be worse.__
[center]#__Темні закляття?__#

// 1498
//__Who you gonna call?__
[center]#__Хто прийде на поміч?__#

// 1499
//“HARRY POTTER!” There were a lot more voices shouting it this time.
-- ГАРРІ ПОТТЕР! -- тепер волали й додаткові голоси.

// 1500
//The Weasley Horrors went off into an extended wailing, now accompanied by some
//of the older Muggleborns, who had produced their own tiny devices, Transfigured
//out of the school silverware no doubt. As their music reached its anticlimax,
//Harry Potter shouted:
Пострахи Візлі й далі видавали затяжний репет, що його тепер підхопили деякі
старшокурсники маґлівського роду, створивши собі такі самі невеличкі пристрої
-- без сумніву, трансфігурували зі шкільного столового срібла. Коли музика майже повністю
вщухла, Гаррі Поттер вигукнув:

// 1501
//__I ain’t afraid of Dark Lords!__
-- __Темних лордів я не боюся!__

// 1502
//There was cheering then, especially from the Gryffindor table, and more
//students produced their own antimusical instruments. The hideous buzzings
//redoubled in volume and built to another awful crescendo:
Залу заполонили аплодисменти, особливо з боку ґрифіндорського стола, і ще
більша кількість учнів створила власні антимузичні інструменти. Огидне
дзижчання залунало вдвічі гучніше, посилюючись у страхітливому крещендо.

// 1503
//__I ain’t afraid of Dark Lords!__
-- __Темних лордів я не боюся!__

// 1504
//Minerva glanced to both sides of the Head Table, afraid to look but with all
//too good a notion of what she would see.
Мінерва з пострахом поглянула в обидва боки вчительського стола, занадто добре
знаючи, що саме вона побачить.

// 1505
//Trelawney frantically fanning herself, Flitwick looking on with curiosity,
//Hagrid clapping along to the music, Sprout looking severe, and Quirrell gazing
//at the boy with sardonic amusement. Directly to her left, Dumbledore humming
//along; and directly to her right, Snape gripping his empty wine goblet,
//white-knuckled, so hard that the thick silver was slowly deforming.
// часткова копія 1300
// різниця 1300/1505
// silver was slowly deforming/thick silver was slowly deforming
// Hagrid clapping/Hagrid clapping along to the music
// Vector and Sinistra bemused/ --
// Quirrell gazing vacuously at nothing / Quirrell gazing at the boy with sardonic amusement
// Albus smiling benevolently. / Directly to her left, Dumbledore humming along;
// And Severus Snape / and directly to her right, Snape
Трелоні несамовито обмахувалася, Флитвік зацікавлено спостерігав за подіями,
Геґрід плескав у такт музиці, Спраут суворо роздивлялася, Квірел із глузливою втіхою
втупився в хлопця. Одразу ліворуч від неї мугикав Дамблдор, а праворуч Снейп
учепився побілілими пальцями у свій порожній келих для вина так сильно,
що товсте срібло повільно деформувалося.

// 1506
//__Dark robes and a mask?__
[center]#__Темні маски та вбрання?__#

// 1507
//__Impossible task?__
[center]#__Неможливі завдання?__#

// 1508
//__Who you gonna call?__
[center]#__Хто прийде на поміч?__#

// 1509
//__HARRY POTTER!__
[center]#__ГАРРІ ПОТТЕР!__#

// 1510
//__Giant Fire-Ape?__
[center]#__Чари некромантії?__#

// 1511
//__Old bat in a cape?__
[center]#__Старий кажан у мантії?__#

// 1512
//__Who you gonna call?__
[center]#__Хто прийде на поміч?__#

// 1513
//__HARRY POTTER!__
[center]#__ГАРРІ ПОТТЕР!__#

// 1514
//Minerva’s lips set in a white line. She would have words with Them about that
//last verse, if They thought she was powerless because it was the first day of
//school and Gryffindor had no points to take away. If They didn’t care about
//detentions then she would find something else.
// Майже повна копія 1303
// Різниця 1303/1514:
// Weasley Horrors/Them
// last part/last verse
// they/They
Губи Мінерви зціпилися в білу лінію. Вона ще поговорить із НИМИ щодо цього останнього
куплету, дарма ВОНИ вважають її безсилою через те, що сьогодні перший день
навчального року, тож Ґрифіндор не має очок, що їх можна було б зняти. Якщо ЇМ начхати
на відпрацювання, вона вигадає щось інше.

// 1515
//Then, with a sudden gasp of horror, she looked in Snape’s direction,
//__surely__ he realised the Potter boy must have no idea who that was
//talking about -
// копія 1304
Раптово затамувавши подих від жаху, вона поглянула в бік Снейпа.
__Звісно__, він мусив розуміти, що маленький Поттер і гадки не мав,
про кого йшлося...

// 1516
//Snape’s face had gone beyond rage into a kind of pleasant indifference. A faint
//smile played about his lips. He was looking in the direction of Harry Potter,
//not the Gryffindor table, and his hands held the crumpled remains of a former
//wine goblet…
// копія 1305 окрім Severus/Snape
Вираз обличчя Снейпа перетнув межу люті й набув відрадної байдужости. Легка усмішка
грала на його губах. Він дивився в бік Гаррі Поттера, а не ґрифіндорського
стола, а його руки досі стискали пожмакані залишки того, що колись було келихом
для вина.

// 1517
//And Harry walked forwards, sweeping his arms and legs through the motions of
//the Ghostbusters dance, keeping a smile on his face. It was a great setup, had
//caught him completely by surprise. The least he could do was play along and not
//ruin it all.
А Гаррі з усмішкою на обличчі рухався вперед, вимахуючи руками й ногами згідно з
рухами танцю "`Мисливців на привидів`". Це була чудово поставлена витівка, що захопила
його цілковито зненацька. Найменше, що він міг зробити, -- підіграти їм і не зіпсувати
цього всього.

// 1518
//Everyone was cheering him. It made him feel all warm inside and sort of awful
//at the same time.
Усі радо вітали його. Через це йому стало тепло на душі, але водночас він почувався
жахливо.

// 1519
//They were cheering him for a job he’d done when he was one year old. A job he
//hadn’t really finished. Somewhere, somehow, the Dark Lord was still alive.
//Would they have been cheering quite so hard, if they knew that?
//копія 1307
Люди плескали йому за те, що він здійснив, коли мав лише рік. За те, чого він навіть
не довів до кінця. Десь там у якийсь спосіб Темний Лорд досі животів. Чи вітали б вони
його так само радісно, якби знали про це?

// 1520
//But the Dark Lord’s power __had__ been broken once.
//копія 1308
Проте силу Темного Лорда вже було знищено одного разу.
// 1521
//And Harry would protect them again. If there was in fact a prophecy and that
//was what it said. Well, actually regardless of what any darn prophecy said.
// копія 1309
І Гаррі захистить їх знову. Якщо пророцтво справді існувало, і в ньому справді
про це йшлося. Та й узагалі -- незалежно від того, що там стверджує якесь кляте пророцтво.

// 1522
//All those people believing in him and cheering him - Harry couldn’t stand to
//let that be false. To flash and fade like so many other child prodigies. To be
//a disappointment. To fail to live up to his reputation as a symbol of the
//Light, never mind __how__ he’d gotten it. He would absolutely, positively,
//no matter how long it took and even if it killed him, fulfill their
//expectations. And then go on to __exceed__ those expectations, so that
//people wondered, looking back, that they had once asked so little of him.
//копія 1310
Всі ці люди вірили в нього й підтримували його -- Гаррі не міг допустити, щоб усе це
було даремно. Спалахнути й згаснути, як багато інших юних вундеркіндів. Виявитися розчаруванням.
Зазнати невдачі, не виправдати репутації символу Світла -- __байдуже__, як він її здобув.
Він безперечно, безумовно, хай скільки часу на це потрібно, навіть якщо це його вбʼє,
виправдає їхні сподівання. А згодом __перевершить__ ці сподівання, і люди, згадуючи
минуле, дивуватимуться, що очікували від нього так мало.

// 1523
//And he shouted out the lie that he’d invented because it scanned well and the
//song called for it:
І він вигукнув вигадану ним брехню, оскільки вона потрапляла в ритм, а пісня потребувала її:

// 1524
//__I ain’t afraid of Dark Lords!__
[center]#__Темних лордів я не боюся!__#

// 1525
//__I ain’t afraid of Dark Lords!__
[center]#__Темних лордів я не боюся!__#

// 1526
//Harry took his last steps toward the Sorting Hat as the music ended. He swept a
//bow to the Order of Chaos at the Gryffindor table, and then turned and swept
//another bow to the other side of the hall, and waited for the applause and
//giggling to die away…
//часткова копія 1312 
Гаррі зробив останні кілька кроків до Сортувального Капелюха, коли музика стихла.
Уклонився Орденові Хаосу за ґрифіндорським столом, повернувся й уклонився іншій частині
зали, а тоді зачекав, щоб оплески й хихотіння вщухли...

***

// 1527
//OMAKE FILES #3: Alternate Endings of ‘Self-Awareness’
// hedrok: TODO на жаль, тут asciidoctor html-коди не дозволяє чомусь.
.ОМАКЕ-ФАЙЛ №3: Альтернативні закінчення розділу «Самоусвідомлення»

// 1528
//The offer to tell the whole plot to anyone who guessed what ‘has never happened
//before’ spurred a __lot__ of interesting attempts. The first omake below
//is taken directly from my personal favorite answer, by Meteoricshipyards. The
//second is based on Kazuma’s suggestion for what “has never happened before”,
//the third on a combination of yoyoente and dougal74, the fourth on wolf550e’s
//review of chapter 10. The one that starts with ‘K’, and the one just above
//that, are from DarkHeart81. The others are my own. Anyone who wants to pick up
//one of my own ideas and run with them, particularly the last one, is welcome to
//do so. And before I get 100 indignant complaints, yes, I am well aware that the
//legislative body of the UK is the House of Commons in Parliament.
Пропозиція розповісти весь сюжет будь-кому, хто здогадається, чого "`ніколи раніше не
траплялося`", заохотила __багато__ цікавих спроб. Перше омаке серед тих, що наведено
нижче, взято безпосередньо з моєї улюбленої відповіді -- від Meteoricshipyards. Наступне
ґрунтується на здогадці Kazuma щодо того, що "`ніколи раніше не траплялося`"; третє є
комбінацією припущень yoyoente та dougal74; четверте -- огляд десятого розділу від wolf550e.
Те, що починається з "`Е`", і ще одне на рядок вище -- від DarkHeart81. Всі решта -- мої.
Якщо когось цікавить самотужки розвивати котрусь із моїх ідей (особливо останню)
-- прошу. І перш ніж я отримаю сотню обурених скарг -- так, мені добре відомо, що
законодавчим органом Великої Британії є палата громад у парламенті.

***

// 1529
//…In the back of his mind, he wondered if the Sorting Hat was genuinely
//__conscious__ in the sense of being aware of its own awareness, and if so,
//whether it was satisfied with only getting to talk to eleven-year-olds once per
//year. Its song had implied so: __Oh, I’m the Sorting Hat and I’m okay, I
//sleep all year and I work one day…__
// копія 1313 - дужки + ...
...Водночас він зацікавився, чи Сортувальний Капелюх справді був
__притомним__, тобто усвідомлював свою свідомість, і якщо так, то чи вистачало
йому розмовляти з одинадцятирічними раз на рік. Пісня ніби підтверджувала
це: "`О, я Сортувальний Капелюх, і в мене все гаразд, я сплю весь рік і працюю
лише раз...`".

// 1530
//When there was once more silence in the room, Harry sat on the stool and
//__carefully__ placed onto his head the 800-year-old telepathic artefact of
//forgotten magic.
// копія 1314
Коли знову запала тиша, Гаррі сів на ослінчик і __обережно__ поклав
собі на голову вісімсотрічний телепатичний артефакт забутої магії.

// 1531
//Thinking, just as hard as he could: __Don’t Sort me yet! I have questions
//I need to ask you! Have I ever been Obliviated? Did you Sort the Dark Lord when
//he was a child and can you tell me about his weaknesses? Can you tell me why I
//got the brother wand to the Dark Lord’s? Is the Dark Lord’s ghost bound to my
//scar and is that why I get so angry sometimes? Those are the most important
//questions, but if you’ve got another moment can you tell me anything about how
//to rediscover the lost magics that created you?__
// копія 1315, 1542
Думаючи якомога сильніше: "`Не сортуй мене відразу! Я маю питання, що їх
треба поставити! Чи забуттятили мене? Чи сортував ти Темного Лорда, коли
він був дитиною, і чи можеш розповісти мені про його слабини? Чи відомо
тобі, чому я отримав сестру палички Темного Лорда? Чи привʼязаний
привид Темного Лорда до мого шраму й чи тому я такий злий подеколи? Це
найважливіші питання, проте якщо ти маєш ще трохи часу, то чи можеш
сказати щось про те, як віднайти втрачені чари, що дали змогу створити тебе?`".

// 1532
//And the Sorting Hat answered, “__No. Yes. No. No. Yes and no, next time
//don’t ask double questions. No.__” and out loud, “RAVENCLAW!”
І Сортувальний Капелюх відповів:

-- Ні. Так. Ні. Ні. Так і ні, і більше не став мені подвійних питань. Ні.

А потім уголос:

-- РЕЙВЕНКЛОВ!

***

// 1533
//__“Oh, dear. This has never happened before…”__
-- От лихо! Такого ніколи раніше не траплялося...

// 1534
//__What?__
"`Що?`".

// 1535
//__“I’m allergic to your hair shampoo -”__
-- У мене алергія на твій шампунь для волосся...

// 1536
//And then the Sorting Hat sneezed, with a mighty “A-CHOO!” that echoed around
//the Great Hall.
Сортувальний Капелюх чхнув надзвичайно гучним «АЧХИ», що відлунням прокотилося
Великою залою.

// 1537
//“Well!” Dumbledore cried jovially. “It seems Harry Potter has been sorted into
//the new House of Achoo! McGonagall, you can serve as the Head of House Achoo.
//You’d better hurry up on making arrangements for Achoo’s curriculum and
//classes, tomorrow is the first day!”
-- Ну що ж! -- весело вигукнув Дамблдор. -- Гаррі Поттер потрапив до нового гуртожитку
-- Ачхи! Макґонеґел, ви станете його вихователькою. Вам краще поквапитися й вжити заходів
щодо підготовки розкладу й навчального плану для Ачхи, завтра перший день занять!

// 1538
//“But, but, but,” stammered McGonagall, her mind in nearly complete disarray,
//“who will be Head of House Gryffindor?” It was all she could think of, she
//__had__ to stop this somehow…
-- Але ж... але... хіба ж... -- затиналася Макґонеґел, цілковито збита з пантелику.
-- Хто ж тоді буде вихователем Ґрифіндору?

Це був єдиний аргумент, що наразі спав їй на
думку, вона __мала__ була якось зупинити це...

// 1539
//Dumbledore put a finger to his cheek, looking thoughtful. “Snape.”
Дамблдор задумливо потер щоку пальцем:

-- Снейп.

// 1540
//Snape’s screech of protest nearly drowned out McGonagall’s, “Then who will be
//Head of __Slytherin?__”
Снейпів заперечливий вереск майже заглушив Макґонеґелів:

-- А хто ж тоді стане вихователем __Слизерину__?

// 1541
//“Hagrid.”
-- Геґрід.

***

// 1542
//__Don’t Sort me yet! I have questions I need to ask you! Have I ever been
//Obliviated? Did you Sort the Dark Lord when he was a child and can you tell me
//about his weaknesses? Can you tell me why I got the brother wand to the Dark
//Lord’s? Is the Dark Lord’s ghost bound to my scar and is that why I get so
//angry sometimes? Those are the most important questions, but if you’ve got
//another moment can you tell me anything about how to rediscover the lost magics
//that created you?__
// копія 1315, 1531
"`Не сортуй мене відразу! Я маю питання, що їх треба поставити! Чи забуттятили
мене? Чи сортував ти Темного Лорда, коли він був дитиною, і чи можеш розповісти
мені про його слабини? Чи відомо тобі, чому я отримав сестру палички Темного
Лорда? Чи привʼязаний привид Темного Лорда до мого шраму й чи тому я такий злий подеколи?
Це найважливіші питання, проте якщо ти маєш ще трохи часу, то чи можеш
сказати щось про те, як віднайти втрачені чари, що дали змогу створити тебе?`".

// 1543
//There was a brief pause.
Запала недовга павза.

// 1544
//__Hello? Do I need to repeat the questions?__
"`Агов? Мені повторити запитання?`".

// 1545
//The Sorting Hat screamed, an awful high-pitched sound that echoed through the
//Great Hall and caused most of the students to clap their hands over their ears.
//With a desperate yowl, it leapt off Harry Potter’s head and bounded across the
//floor, pushing itself along with its brim, and made it halfway to the Head
//Table before it exploded.
Сортувальний Капелюх заволав жахливим високим криком, що відлунням прокотився
Великою залою й змусив більшість учнів затиснути вуха долонями. Капелюх відчайдушно
завив, злетів із голови Гаррі Поттера, поплигав геть, відштовхуючись крисами
від підлоги, і вибухнув, не подолавши й половини шляху до вчительського стола.

***

// 1546
//“SLYTHERIN!”
-- СЛИЗЕРИН!

// 1547
//Seeing the look of horror on Harry Potter’s face, Fred Weasley thought faster
//than he ever had in his life. In a single motion he whipped out his wand,
//whispered __“Silencio!”__ and then “__Changemyvoiceio!”__ and finally
//“__Ventriliquo!__”
Вираз жаху на Гарріному обличчі змусив Фреда Візлі думати так швидко, як ніколи в житті,
й одним рухом витягнути паличку.

-- __Сіленціо!__ -- прошепотів він. А тоді: -- __Голосозмініус! Черевомовліо!__

// 1548
// Копія 1321
//“Just kidding!” said Fred Weasley. “GRYFFINDOR!”
-- Жартую-жартую! -- сказав Фред Візлі. -- ҐРИФІНДОР!

***

// 1549
//__“Oh, dear. This has never happened before…”__
-- От лихо! Такого ніколи раніше не траплялося...

// 1550
//__What?__
"`Що?`"

// 1551
//__“Ordinarily I would refer such questions to the Headmaster, who could
//ask me in turn, if he wished. But some of the information you’ve asked for is
//not only beyond your own user level, but beyond the Headmaster’s.”__
-- Зазвичай я спрямовую з такими питаннями до директора, що й собі
міг би запитати мене, якщо забажає. Але частина інформації з вашого запиту закрито не
тільки для вашого рівня доступу, але й для директорського.

// 1552
//__How can I raise my user level?__
"`Як я можу підвищити свій рівень доступу?`".

// 1553
//__“I’m afraid I am not allowed to answer that question at your current
//user level.”__
-- На жаль, ваш нинішній рівень доступу не дозволяє мені дати відповіді
на цей запит.

// 1554
//__What options __are__ available at my user level?__
"`Які дії __доступні__ для мого рівня доступу?`".

// 1555
//After that it didn’t take long -
Далі все відбулося швидко...

// 1556
//“ROOT!”
-- ROOT!

***

// 1557
//__“Oh, dear. This has never happened before…”__
-- От лихо! Такого ніколи раніше не траплялося...

// 1558
//__What?__
"`Що?`".

// 1559
//__“I’ve had to tell students before that they were mothers - it would
//break your heart to know what I saw in their minds - but this is the first time
//I’ve ever had to tell someone they were a father.”__
-- Мені доводилося розказувати ученицям, що вони були при надії, -- якби ти знав, що я бачив
у їхніх головах, це б розбило тобі серце. Та я вперше мушу повідомити комусь,
що він стане батьком.

// 1560
//__WHAT?__
"`ЩО?`".

// 1561
//__“Draco Malfoy is carrying your baby.”__
-- Драко Мелфой носить твою дитину.

// 1562
//__WHAAAAAAAT?__
"`Щ-О-О-О-О-О-О-О?`".

// 1563
//__“To repeat: Draco Malfoy is carrying your baby.”__
-- Повторюю: Драко Мелфой носить твою дитину.

// 1564
//__But we’re only eleven -__
"`Але нам усього одинадцять...`".

// 1565
//__“Actually, Draco is secretly thirteen years old.”__
-- Насправді, Драко вже тринадцять років.

// 1566
//__B-b-but men can’t get pregnant -__
"`Ал-л-ле чоловік не може завагітніти...`".

// 1567
//__“And a girl under those clothes.”__
-- А ще вона дівчина, тільки приховує це під вбранням.

// 1568
//__BUT WE’VE NEVER HAD SEX, YOU IDIOT!__
"`АЛЕ В НАС НІКОЛИ НЕ БУЛО СЕКСУ, ДУРНЮ!`".

// 1569
//__“SHE OBLIVIATED YOU AFTER THE RAPE, MORON!”__
-- ВОНА ЗАБУТТЯТНУЛА ТЕБЕ ПІСЛЯ ЗҐВАЛТУВАННЯ, НЕДОУМКУ!

// 1570
//Harry Potter fainted. His unconscious body fell off the stool with a dull thud.
Гаррі Поттер зомлів. Його непритомне тіло з глухим звуком гепнулося з ослінчика.

// 1571
//“RAVENCLAW!” called out the Hat from where it lay on top of his head. That had
//been even funnier than its first idea.
-- РЕЙВЕНКЛОВ! -- вигукнув Капелюх, що й досі лежав на маківці Гаррі. Цей жарт
виявився навіть смішнішим за той, що спав йому на думку спершу.

***

// 1572
//“ELF!”
-- ЕЛЬФ!

// 1573
//Huh? Harry remembered Draco mentioning a ‘House Elf’, but what was that
//exactly?
Га? Гаррі пам’ятав, як Драко говорив щось про гуртожиток ельфів-домовиків, але
що саме це означало?

// 1574
//Judging by the appalled looks dawning on the faces around him, it wasn’t
//anything good -
З огляду на приголомшені обличчя навколо -- нічого доброго...

***

// 1575
//“PANCAKES!”
-- ВІЛЬНА КАСА!
// @ http://www.ihop.com/

***

// 1576
//“REPRESENTATIVES!”
-- МАЖОРИТАРКА!

***

// 1577
//__“Oh, dear. This has never happened before…”__
-- От лихо! Такого ніколи раніше не траплялося...

// 1578
//__What?__
"`Що?`".

// 1579
//__“I’ve never Sorted someone who was a reincarnation of Godric Gryffindor
//AND Salazar Slytherin AND Naruto.”__
-- Мені ще не доводилося визначати гуртожиток для когось, хто був би
реінкарнацією і Ґодрика Ґрифіндора, і Салазара Слизерина, і Наруто.

***

// 1580
//“ATREIDES!”
// hedrok: Дюна, переклад Анатолія Пітика й Катерини Грицайчук, КСД
-- АТРІД!

***

// 1581
//“Fooled you again! HUFFLEPUFF! SLYTHERIN! HUFFLEPUFF!”
-- Знову надурив! ГАФЕЛПАФ! СЛИЗЕРИН! ГАФЕЛПАФ!

***

// 1582
//“PICKLED STEWBERRIES!”
-- МАРИНОВАНА ПОЛІНИЦЯ!
// ! чи є кращі пропозиції? Можемо заміняти чим завгодно, бо це має бути totally random
// ! http://lesswrong.com/lw/edz/pickled_stewberries_in_hpmor_omake_3/

***

// 1583
//“KHAAANNNN!”
-- Х-А-А-А-А-А-А-Н!

***

// 1584
//At the Head Table, Dumbledore went on smiling benignly; small metallic sounds
//occasionally came from Snape’s direction as he idly compacted the twisted
//remains of what had once been a heavy silver wine goblet; and Minerva
//McGonagall clenched the podium in a white-knuckled grip, knowing that Harry
//Potter’s contagious chaos had infected the Sorting Hat itself.
//часткова копія 1436
За вчительським столом Дамблдор і далі лагідно всміхався. З боку Снейпа
подеколи долинало тихе металеве дзеленчання: він ліниво перебирав погнутими рештками
того, що раніше було важким срібним келихом для вина. Мінерва Макґонеґел вчепилася в
катедру побілілими пальцями, знаючи, що заразний хаос Гаррі Поттера
якось пробрався й у Сортувальний Капелюх...

// 1585
//Scenario after scenario played out through Minerva’s head, each worse than the
//last. The Hat would say that Harry was too evenly balanced between Houses to
//Sort, and decide that he belonged to all of them. The Hat would proclaim that
//Harry’s mind was too strange to be Sorted. The Hat would demand that Harry be
//expelled from Hogwarts. The Hat had gone into a coma. The Hat would insist that
//a whole new House of Doom be created just to accomodate Harry Potter, and
//__Dumbledore would make her do it…__
У голові Мінерви миготіли сценарій за сценарієм, один від одного гірший. Капелюх вирішить,
що Гаррі буде однаково добре в усіх гуртожитках, а тому належатиме до них усіх
одночасно. Капелюх оголосить, що розум Гаррі занадто дивний для сортування. Капелюх
вимагатиме вигнати Гаррі Поттера з Гоґвортсу. Капелюх впаде в кому. Капелюх наполягатиме,
що для потреб Гаррі Поттера необхідно створити новий гуртожиток Лихої Долі,
і __Дамблдор змусить її це зробити__...

// 1586
//Minerva remembered what Harry had told her in that disastrous trip to Diagon
//Alley, about the… planning fallacy, she thought it had been… and how people
//were usually too optimistic, even when they thought they were being
//pessimistic. It was the sort of information that preyed on your mind, dwelling
//in it and spinning off nightmares…
Мінерва згадала, що саме Гаррі розповів їй під час тієї катастрофічної прогулянки
алеєю Діаґон щодо... омани планування, здається... Що люди зазвичай занадто оптимістичні,
навіть коли вважають, що мислять песимістично. Це був той тип інформації, який міцно
засідає в голові й не дає спокою, обертається жахіттями...

// 1587
//But what was the __worst__ that could happen?
Але що могло трапитися в __найгіршому__ разі?

// 1588
//Well… in the __worst-case scenario,__ the Hat would assign Harry to a
//whole new House. Dumbledore would insist that she do it - create a whole new
//House just for him - and she’d have to rearrange all the class schedules on the
//first day of term. And Dumbledore would remove her as Head of House Gryffindor,
//and give her beloved House over to… Professor Binns, the History ghost; and she
//would be assigned as Head of Harry’s House of Doom; and she would futilely try
//to give the child orders, deducting point after point without effect, while
//disaster after disaster was blamed on her.
// @ worst-case scenario - ми завжди казали просто worst-case, не перекладали :[
Що ж... У __найгіршому разі__ Капелюх призначить Гаррі до цілковито нового гуртожитку.
Дамблдор наполягатиме на тому, що вона має взятися до цього завдання -- створення нового
гуртожитку особисто для нього, -- і їй доведеться реорганізовувати весь навчальний розклад
першого ж дня навчання. Дамблдор усуне її з посади виховательки Ґрифіндору й передасть її
любий гуртожиток... професорові Бінсу, привидові з історії. Її ж призначать вихователькою
гуртожитку Лихої Долі Гаррі. І вона безуспішно намагатиметься давати цій дитині
накази, без жодного ефекту зніматиме з нього очки, а кожним новим нещастям дорікатимуть саме їй.

// 1589
//Was that the worst-case scenario?
Чи було це найнесприятливішим сценарієм?

// 1590
//Minerva honestly didn’t see how it could be any worse than that.
Мінерва щиро не могла уявити нічого гіршого за це.

// 1591
//And even in the very worst case - no matter __what__ happened with Harry -
//it would all be over in seven years.
І навіть у най-найгіршому разі -- незалежно від того, __що саме__ трапиться
з Гаррі, -- це все закінчиться за сім років.

// 1592
//Minerva felt her knuckles slowly relax their white-knuckled grip on the podium.
//Harry had been right, there was a kind of comfort in staring directly into the
//furthest depths of the darkness, knowing that you had confronted your worst
//fears and were now prepared.
Вона відчула, як її пальці, що досі стискали катедру, повільно розслабилися.
Гаррі мав рацію: була якась розрада в тому, щоб пильно поглянути в найвіддаленіші
глибини темряви, осягнути, що ти зазирнув у вічі своїм найгіршим страхам і тепер
готовий до зустрічі з ними.

// 1593
//The frightened silence was broken by a single word.
//часткова копія 1453
Налякану тишу розітнуло єдине слово.

// 1594
//“Headmaster!” called the Sorting Hat.
-- Директор! -- вигукнув Сортувальний Капелюх.

// 1595
//At the Head Table, Dumbledore rose, his face puzzled. “Yes?” he addressed the
//Hat. “What is it?”
// ! Українською тут не виходить двозначності. Якби Капелюх звертався до Дамблдора,
// ! було б "Директоре"
// ! Тому він віповідає "Що", а не "Так", просто здивованість
// ! І тому ми трохи змінили відповідь Капелюха
Дамблдор спантеличено підвівся з-за вчительського стола.

-- Що? -- спитав він Капелюха. -- Що таке?

// 1596
//“I wasn’t talking to you,” said the Hat. “I was Sorting Harry Potter into the
//place in Hogwarts where he most belongs, namely the Headmaster’s office -”
-- Це я не вам, -- відказав Капелюх. -- Я визначив місце в Гоґвортсі
для Гаррі Поттера, де він найкраще себе проявить: посаду директора.
