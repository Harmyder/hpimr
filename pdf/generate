#!/usr/bin/env bash

# виходити після будь-якої помилки:
set -e

# друкувати на stderr команди, які виконуються:
set -x

BASEDIR="$(dirname "$0")"       # директорія цього скрипта
DESTDIR="../public_html/files"

TITLE="Гаррі Поттер і Методи Раціональности"

function build_pdf {
    PDFTEX="$1"
    FIRST_CHAPTER="$2"
    LAST_CHAPTER="$3"
    AFTER_CHAPTERS="$4"
    PDFTITLE="${5:-$TITLE}"

    echo "$PDFTITLE" > pdftitle.tex

    CHAPTERS="$(seq -f "%03.0f.xml" $FIRST_CHAPTER $LAST_CHAPTER) $AFTER_CHAPTERS"

    # TODO: metadata-file
    # latex-шаблон тут: https://github.com/jgm/pandoc/blob/master/data/templates/default.latex
    pandoc --from=docbook $CHAPTERS \
        --include-in-header=header.tex \
        --metadata-file=../metadata.yaml \
        -M "title:$PDFTITLE" \
        --variable="documentclass:book" \
        --variable=subparagraph \
        --variable="linestretch:1.2" \
        --variable="fontfamily:noto" \
        --variable="fontsize:10pt" \
        --variable="hyperrefoptions:pdfusetitle" \
        -o "$PDFTEX"

        # те, що не працює:
        #--variable="CJKmainfont:Noto Serif CJK SC" \
        #--variable="mainfont:Noto Serif" \

    # заміняємо \maketitle на включення front.tex:
    sed -i "0,/maketitle/ s/\\\\maketitle.*$/\\\\input{front}/" "$PDFTEX"

    # лагодимо апостроф на такий, який розуміє tex:
    sed -i "s/ʼ/'/g" "$PDFTEX"
    # лагодимо наголоси:
    sed -i "s|\(.\)\\\\{stress\\\\}|\\\\'\\1|g" "$PDFTEX"

    # лапки-ялинки:
    sed -i "$PDFTEX" \
        -e 's/``/<</g' \
        -e 's/\x27\x27/>>/g'

    # віднвлюємо заглушки для деяких тегів docbook:
    sed -i "$PDFTEX" \
        -e 's|@@@hr@@@|\\vspace{1em} \\hrule \\vspace{1em}|g' \
        -e 's|@@@center@@@|\\centerline{|g' \
        -e 's|@@@underline@@@|\\underline{|g' \
        -e 's|@@@end@@@|}|g'

    # робимо кінцеву примітку автора на окремій сторінці:
    sed -i 's/\\emph{Оригінальна примітка автора:}/\\pagebreak \\emph{Оригінальна примітка автора:}/' "$PDFTEX"

    # повторюємо двічі, щоб latex міг згенерувати зміст:
    xelatex -interaction=nonstopmode "$PDFTEX" >xelatex0.log
    xelatex -interaction=nonstopmode "$PDFTEX" >xelatex1.log
}

function build_epub {
    # робимо копію:
    rm -rf tmp-copy && mkdir tmp-copy
    cp -r *.xml tmp-copy

    # матеріалізуємо зірочки для epub
    sed -i "s|<?asciidoc-hr?>|<?asciidoc-hr?>* * *|g" ./*.xml
    # наголоси
    sed -i "s|{stress}|\\&#x0301;|g" ./*.xml
    # лапки
    sed -i "s|<quote>|\\&laquo;|g" ./*.xml
    sed -i "s|</quote>|\\&raquo;|g" ./*.xml
    #./[01]*.xml 
    pandoc --from=docbook --to=epub3 --metadata-file=../metadata.yaml ./short-intro.xml [01]*.xml technical.xml -o hpimr.epub
    mkdir -p $DESTDIR/
    mv hpimr.epub $DESTDIR/

    # повертаємо xml
    rm *.xml
    mv tmp-copy/*.xml .
    rmdir tmp-copy
}

cd "$BASEDIR"
mkdir -p "$DESTDIR"

# хеш:
if test "$CI_COMMIT_SHA" ; then
    echo "$CI_COMMIT_SHA" > githash.tex
else
    git rev-parse HEAD > githash.tex
fi

# генеруємо docbook:
asciidoctor --backend=docbook --destination-dir=. ../*.asc

build_epub

# матеріалізуємо горизонтальні лінії, щоб вони пережили конвертацію в tex:
sed -i 's|<?asciidoc-hr?>|@@@hr@@@|g' ./*.xml

# center та underline
sed -i ./*.xml \
  -e 's|<phrase role="center">|@@@center@@@|g' \
  -e 's|<phrase role="underline">|@@@underline@@@|g' \
  -e 's|</phrase>|@@@end@@@|g' \
  -e 's|<center>|@@@center@@@|g' \
  -e 's|</center>|@@@end@@@|g'

# уникнення зліплень слів у вкладених <emphasis>
  #-e 's|<emphasis>| <emphasis> |g' \
  #-e 's|</emphasis>| </emphasis> |g' \

# шість книжок:
build_pdf hpimr1.tex 1 21 "help.xml" "Гаррі Джеймс Поттер-Еванс-Веррес і Методи Раціональности"
mv hpimr1.pdf $DESTDIR/

build_pdf hpimr2.tex 22 37 "help.xml" "Гаррі Джеймс Поттер-Еванс-Веррес та Ігри Професора"
mv hpimr2.pdf $DESTDIR/

build_pdf hpimr3.tex 38 64 "help.xml" "Гаррі Джеймс Поттер-Еванс-Веррес і Тіні Смерти"
mv hpimr3.pdf $DESTDIR/

build_pdf hpimr4.tex 65 85 "help.xml" "Герміона Джін Ґрейнджер і Поклик Фенікса"
mv hpimr4.pdf $DESTDIR/

build_pdf hpimr5.tex 86 99 "help.xml" "Гаррі Джеймс Поттер-Еванс-Веррес і Останній Ворог"
mv hpimr5.pdf $DESTDIR/

build_pdf hpimr6.tex 100 122 "help.xml" "Гаррі Джеймс Поттер-Еванс-Веррес і Філософський Камінь"
mv hpimr6.pdf $DESTDIR/

# повна книжка:
build_pdf hpimr.tex 1 122 "help.xml"
mv hpimr.pdf $DESTDIR/
