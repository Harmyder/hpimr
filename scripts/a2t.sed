#!/usr/bin/env -S sed -f
s#^//.*$##g
s/--/—/g
s/__//g
s/**//g
s/#//g
s/+++<center>+++//g
s/+++<\/center>+++//g
s/\[[a-z]\+\]//g
s/{stress}//g
s/`"/"/g
s/"`/"/g
s/\([^\b"]\)'\([^\b"]\)/\1ʼ\2/g
s/`//g
